create table user_game (
	id INT,
	uuid INT,
	username VARCHAR(50) NOT NULL,
	password VARCHAR(50) NOT NULL,
);

create table user_game_biodata (
	id INT,
	uuid INT,
	name VARCHAR(50) NOT NULL,
	email VARCHAR(50) NOT NULL,
	gender VARCHAR(50) NOT NULL,
	date_of_birth DATE,
	city VARCHAR(50)
);

create table user_game_history (
    id INT,
	uuid INT,
	name_biodata BIGINT REFERENCES user_game_biodata(name),
	username_game BIGINT REFERENCES user_game(username),
	win VARCHAR (3) NOT NULL,
    lose VARCHAR (3) NOT NULL,
    time_play TIME NOT NULL,
    total_match INT,
    win_rate INT
    UNIQUE (name_biodata, username_game)
);

insert into user_game (id, uuid, username, password) values (1, 3, 'salben0', 'cD5.#>j=88{h7');
insert into user_game (id, uuid, username, password) values (2, 44, 'bfullalove1', 'jA1"4ygO');
insert into user_game (id, uuid, username, password) values (3, 21, 'rgowthorpe2', 'pM5`.PQSk`e<R');
insert into user_game (id, uuid, username, password) values (4, 76, 'igraybeal3', 'fM3@bf2Oi0r*');
insert into user_game (id, uuid, username, password) values (5, 11, 'rcatherick4', 'nZ9%(bStzi');

insert into user_game_biodata (id, uuid, name, email, gender, date_of_birth, city) values (1, 61, 'Lyssa', 'lartiss0@reddit.com', 'Female', '2022-08-21', 'Al Khafsah');
insert into user_game_biodata (id, uuid, name, email, gender, date_of_birth, city) values (2, 75, 'Arron', 'abraithwaite1@blog.com', 'Genderqueer', '2023-03-05', 'Two Hills');
insert into user_game_biodata (id, uuid, name, email, gender, date_of_birth, city) values (3, 28, 'Carce', 'carmour2@opera.com', 'Male', '2022-08-13', 'Depapre');