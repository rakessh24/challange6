create table user_game_biodata (
	id INT,
	uuid INT,
	name VARCHAR(50) NOT NULL,
	email VARCHAR(50)NOT NULL,
	gender VARCHAR(50) NOT NULL,
	date_of_birth DATE,
	city VARCHAR(50)
);
insert into user_game_biodata (id, uuid, name, email, gender, date_of_birth, city) values (1, 61, 'Lyssa', 'lartiss0@reddit.com', 'Female', '2022-08-21', 'Al Khafsah');
insert into user_game_biodata (id, uuid, name, email, gender, date_of_birth, city) values (2, 75, 'Arron', 'abraithwaite1@blog.com', 'Genderqueer', '2023-03-05', 'Two Hills');
insert into user_game_biodata (id, uuid, name, email, gender, date_of_birth, city) values (3, 28, 'Carce', 'carmour2@opera.com', 'Male', '2022-08-13', 'Depapre');
insert into user_game_biodata (id, uuid, name, email, gender, date_of_birth, city) values (4, 76, 'Fawne', 'forry3@webmd.com', 'Female', '2022-12-31', 'Pukou');
insert into user_game_biodata (id, uuid, name, email, gender, date_of_birth, city) values (5, 99, 'Leonerd', 'lmousby4@sciencedaily.com', 'Male', '2023-02-14', 'Biharamulo');
insert into user_game_biodata (id, uuid, name, email, gender, date_of_birth, city) values (6, 40, 'Diana', 'dorteau5@bing.com', 'Female', '2022-08-11', 'Changgang');
insert into user_game_biodata (id, uuid, name, email, gender, date_of_birth, city) values (7, 75, 'Gardiner', 'gmileham6@163.com', 'Male', '2022-12-27', 'Xishanzui');
insert into user_game_biodata (id, uuid, name, email, gender, date_of_birth, city) values (8, 49, 'Silvan', 'sreymers7@issuu.com', 'Male', '2022-10-12', 'Jejkowice');
insert into user_game_biodata (id, uuid, name, email, gender, date_of_birth, city) values (9, 2, 'Aloin', 'ajuszczyk8@xinhuanet.com', 'Male', '2023-01-05', 'Kota Kinabalu');
insert into user_game_biodata (id, uuid, name, email, gender, date_of_birth, city) values (10, 1, 'Cherri', 'cdabner9@shinystat.com', 'Female', '2023-02-27', 'General Levalle');
